import React, { Component } from "react";
import "../index.css";

class Modal extends Component {
  render() {
    let popupStatus = this.props.propStatus;

    document.addEventListener("keydown", (e) => {
      if (e.key == "Escape") {
        this.props.popupClose(false);
      }
    });

    return (
      <div className="popup-screen">
        <div
          onClick={() => {
            this.props.popupClose(false, this.props.country);
          }}
          className="overlay"
        ></div>

        <div className="popup">
          <button
            onClick={() => this.props.popupClose(false, this.props.country)}
            type="button"
            className="popup-btn-close"
          >
            X
          </button>
          <img
            className="popup-image"
            src={this.props.country.flags.png}
            alt="Card image cap"
          />
          <div className="popup-info">
            <div className="popup-country-name">
              <h5 className="card-title">
                <strong>{this.props.country.name.common}</strong>
              </h5>
            </div>
            <ul className="popup-list">
              <li className="popup-list-item">
                <strong>Population:</strong> {this.props.country.population}
              </li>
              <li className="popup-list-item">
                <strong>Region:</strong> {this.props.country.region}
              </li>
              <li className="popup-list-item">
                <strong>Capital:</strong> {this.props.country.capital}
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
