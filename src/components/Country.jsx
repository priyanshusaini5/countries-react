import React, { Component } from "react";
import "../index.css";

class Country extends Component {
  render() {
    // console.log(this.props);
    return (
      <div
        onClick={() => 
          this.props.popupClick(true,this.props.country)
        }
        className="card country"
        style={{ width: "18rem" }}
      >
        <img
          className="card-img-top"
          src={this.props.country.flags.png}
          alt="Card image cap"
        />
        <div className="card-body">
          <h5 className="card-title">
            <strong>{this.props.country.name.common}</strong>
          </h5>
        </div>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">
            <strong>Population:</strong> {this.props.country.population}
          </li>
          <li className="list-group-item">
            <strong>Region:</strong> {this.props.country.region}
          </li>
          <li className="list-group-item">
            <strong>Capital:</strong> {this.props.country.capital}
          </li>
        </ul>
      </div>

    );
  }
}

export default Country;
