import React, { Component } from "react";
import axios from "axios";
import Country from "./Country";
import Navbar from "./Navbar";
import Modal from "./Modal";

class Countries extends Component {
  constructor(props) {
    super(props);

    this.state = {
      //   region: "All",
      countries: [],
      errorMsg: "",
      popupStatus: false,
      popupCountry: {},
    };
  }

  componentDidMount() {
    axios
      .get("https://restcountries.com/v3.1/all")
      .then((response) => {
        // console.log("done", response);
        document.getElementsByClassName("lds-ripple")[0].style.display = "none";
        this.setState({ countries: response.data });
      })
      .catch((error) => {
        // console.log(error);
        document.getElementsByClassName("lds-ripple")[0].style.display = "none";
        this.setState({ errorMsg: "Error retrieving data" });
      });
  }

  handlePopup = (popupStatus, popupCountry) => {
    this.setState({ popupStatus });
    this.setState({ popupCountry });
  };

  handlePopupClose = (popupStatus, popupCountry) => {
    this.setState({ popupStatus });
  };

  

  render() {
    let propsRegion = this.props.region;
    if (propsRegion === "") {
      propsRegion = "All";
    }
    const { countries, errorMsg, popupStatus, popupCountry } = this.state;
    return (
      <div className="countries-container">
        
        {countries.length
          ? countries.map((country) => {
              if (propsRegion === "") {
                <Country
                  key={country.name.official}
                  country={country}
                  popupClick={this.handlePopup}
                  
                />;
              } else if (country.region === propsRegion) {
                return (
                  <Country
                    key={country.name.official}
                    country={country}
                    popupClick={this.handlePopup}
                  />
                );
              } else if (propsRegion === "All") {
                return (
                  <Country
                    key={country.name.official}
                    country={country}
                    popupClick={this.handlePopup}
                  />
                );
              }
            })
          : null}
        {popupStatus ? (
          <Modal popupClose={this.handlePopupClose} country={popupCountry} />
        ) : null}
        {errorMsg ? <div>{errorMsg}</div> : null}
      </div>
    );
  }
}

export default Countries;
