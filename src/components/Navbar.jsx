import React, { Component } from "react";
import "../index.css";

class Navbar extends Component {
  render() {
    console.log(this.props);
    return (
      <nav
        className="navbar"
        style={{ backgroundColor: "#e3f2fd" }}
        data-bs-theme="dark"
      >
        <div className="container-fluid">
          <a className="navbar-brand" id="heading">
            Where in the World?
          </a>
          <form className="d-flex" role="search">
            <input
              className="form-control me-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button type="button" className="btn btn-primary">
              Search
            </button>
          </form>

          <div className="form-floating ">
            <select
              onChange={(event) => this.props.changeRegion(event)}
              className="form-select "
              id="floatingSelect"
              aria-label="Floating label select example"
            >
              <option value="All">All</option>
              <option value="Americas">Americas</option>
              <option value="Africa">Africa</option>
              <option value="Asia">Asia</option>

              <option value="Oceania">Oceania</option>

              <option value="Europe">Europe</option>
            </select>
            <label>Region</label>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navbar;
