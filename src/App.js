import React, { Component } from 'react'
import './App.css';
import Countries from './components/Countries';
import Navbar from './components/Navbar';

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      region: ''
    }
  }

  // console.log(this.state.region)

  handleChangeRegion = (e) => {
    let newRegion = e.target.value
    this.setState({ region: newRegion })
  }

  render() {
    return (

      <div>

        <Navbar changeRegion={this.handleChangeRegion} />
        <div className="lds-ripple">
        <div></div>
        <div></div>
      </div>
        <Countries  region={this.state.region} />
      </div>
    )
  }
}

export default App

